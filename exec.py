# -*- coding: utf-8 -*-
#Initialize dependencies
from BeautifulSoup import BeautifulSoup
import xlrd
from xlrd import open_workbook,XL_CELL_TEXT
import urllib2
import openpyxl
from openpyxl import load_workbook,workbook
import time
from sets import Set
import sys
from os import listdir
from os.path import isfile, join
import os

##########################################################################################################################################################################
def variables_input_and_globals():
	#defining global variables
	global outputxlsx
	global inputxlsx
	global workbook_writer
	global unique_addresses
	global results_book	
	global workbook_reader
	global worksheet
	global address_col

	#Printing directory contents
	print ""
	pathname = os.path.dirname(sys.argv[0])        
	mypath = str(os.path.abspath(pathname))
	onlyfiles = [ f for f in listdir(mypath) if isfile(join(mypath,f)) ]
	print "Files contained in folder:"
	print ""
	print onlyfiles
	print ""
	
	#Request input xlsx-file
	inputxlsx = raw_input('Input name of the excel-file you want to process: ')
	if inputxlsx.endswith(".xlsx"):
		"input contains proper file-ending"
	else:
		inputxlsx = inputxlsx + ".xlsx"
	print "setting input workbook..."

	#reading input
	workbook_reader = xlrd.open_workbook(str(inputxlsx))
	print "setting worksheet..."
	
	#setting worksheet
	worksheet = workbook_reader.sheet_by_index(0)

	print "loading input to writer..."
	
	#asking for output and setting file-name
	outputxlsx = raw_input('Name the output file (use same name for overwrite): ')
	if outputxlsx.endswith(".xlsx"):
		"input contains proper file-ending"
	else:
		outputxlsx = outputxlsx + ".xlsx"
	
	#loading input to workbook writer
	workbook_writer = load_workbook(filename = inputxlsx)
	
	#Set and dictionary for storage and lookup
	unique_addresses = set()
	results_book = dict()

##########################################################################################################################################################################

def address_lookup_loop():
	#Run loop for all counted addresses in input.xlsx sheet
	for i in range(worksheet.nrows):
		#Set address and zipcode correspondingly
		print "########################################################"
		print "########################################################"
		print "setting address..."
		address = worksheet.cell(i,5)
		print "setting zipcode..."
		zipcode = worksheet.cell(i,6)
		
		#Set url request
		urlstring = "https://www.conzoom.eu/Webservices/GetMosaic.aspx?address=" + unicode(address.value) + "&postcode=" + unicode(zipcode.value) + ""
		#Clear whitespaces and replace with url-correct %20
		urlstring = urlstring.replace(" ", "%20")
		
		#check if we have already processed the address, before looking it up over http
		if str(address)+str(zipcode) not in unique_addresses:
			try:
				url = urllib2.urlopen(unicode(urlstring).encode('utf-8'))
			except urllib2.HTTPError, err:
				if err.code == 404:
					print "Page not found! (404)"
				elif err.code == 403:
					print "Access denied! (403)"
				else:
					print "Something happened! Error code", err.code
			except urllib2.URLError, err:
				print "Some other error happened:", err.reason
			#Get content from Conzoom API
			print "reading url..."
			content = url.read()
			#init soup and find first H1 tag plus remove HTML taggings
			soup = BeautifulSoup(content)
			result = soup.find("h1")
			#unicoding results
			result = unicode(result)
			#removing html from results
			result = result.replace('<h1 class="">', "")
			result = result.replace('</h1>', "")
		#if we already looked up the address we will take it from the dict()
		else:
			result = results_book[str(str(address)+str(zipcode))]
	
		#initiate workbook writer
		ws = workbook_writer.active
		#write results in sheet
		print "writing to active workbook..."
		ws['L'+str(i+1)] = result
		print "Column " + str(i+1) + " of " + str(worksheet.nrows) + " done!"
		
		#if unique, add to dictionary for quick-access
		if str(address)+str(zipcode) not in unique_addresses:
			results_book.update({str(str(address)+str(zipcode)): str(result)})
		unique_addresses.add(str(address)+str(zipcode))
		print "Result for " + str(address.value) + ": " + str(results_book[str(str(address)+str(zipcode))])

##########################################################################################################################################################################

def main():
	#Start timing script
	print "starting timer..."
	start_time = time.time()
	#run the address lookup loop for the file defined in variables_input_and_globals()
	address_lookup_loop()
	print "########################################################"
	print "########################################################"
	print ""
	print "saving workbook..."
	workbook_writer.save(outputxlsx)
	print ""
	print "Number of unique addresses executed: " + str(len(unique_addresses))
	print ""
	print "Total execution time was: " + str(time.time() - start_time) + " seconds"
	print ""
	print "All segments written to " + str(outputxlsx)
	print ""
	print "########################################################"
	print "########################################################"
	
##########################################################################################################################################################################

variables_input_and_globals()
main()